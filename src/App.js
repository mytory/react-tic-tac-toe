import {useState} from 'react';

export default function Game() {
    const [mark, setMark] = useState('X');
    const [history, setHistory] = useState([Array(9).fill(null)]);

    function goBack(index) {
        const newSquares = history[index].slice();
        setHistory([...history, newSquares]);
    }

    return <div className="game">
        <div className="game-board">
            <Board mark={mark} setMark={setMark}
                   history={history} setHistory={setHistory}/>
        </div>
        <div className="game-info">
            <ol>
                {history.map((squares, index) => {
                    return <li key={index}>
                        <button onClick={() => goBack(index)}>
                            {index === 0 ? 'Go to first step' : `Go to ${index + 1} step`}
                        </button>
                    </li>
                })}
            </ol>
        </div>
    </div>
}


function Board({mark, setMark, history, setHistory}) {
    const [winner, setWinner] = useState(null);
    const currentSquares = history[history.length - 1];

    function handleSquareClick(i) {
        if (currentSquares[i] || winner) {
            return;
        }
        const newSquare = currentSquares.slice();
        newSquare[i] = mark;
        setHistory([...history, newSquare]);
        calculateWinner(newSquare)
        setMark(mark === 'X' ? 'O' : 'X');
    }

    function calculateWinner(squares) {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                setWinner(squares[a]);
                return squares[a];
            }
        }
        return null;
    }

    return <>
        <div className="status" style={{display: (winner ? 'block' : 'none')}}>Winner is {winner}</div>
        <div className="status" style={{display: (winner ? 'none' : 'block')}}>Next is {mark}</div>
        <div className="board-row">
            <Square value={currentSquares[0]} onSquareClick={() => handleSquareClick(0)}/>
            <Square value={currentSquares[1]} onSquareClick={() => handleSquareClick(1)}/>
            <Square value={currentSquares[2]} onSquareClick={() => handleSquareClick(2)}/>
        </div>
        <div className="board-row">
            <Square value={currentSquares[3]} onSquareClick={() => handleSquareClick(3)}/>
            <Square value={currentSquares[4]} onSquareClick={() => handleSquareClick(4)}/>
            <Square value={currentSquares[5]} onSquareClick={() => handleSquareClick(5)}/>
        </div>
        <div className="board-row">
            <Square value={currentSquares[6]} onSquareClick={() => handleSquareClick(6)}/>
            <Square value={currentSquares[7]} onSquareClick={() => handleSquareClick(7)}/>
            <Square value={currentSquares[8]} onSquareClick={() => handleSquareClick(8)}/>
        </div>
    </>;
}

function Square({value, onSquareClick}) {
    return <button className="square" onClick={onSquareClick}>{value}</button>;
}

